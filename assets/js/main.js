$(document).ready(function() {

  //Media Queries-----------------------------------------------
	var queries = [
	  {
	    context: 'range_0',
	    match: function() {
	      //responsiveEqualHeight();
	      slideDrawerEnable();
        document.documentElement.id = 'range_0';
        console.log('current range:', MQ.new_context);
	    },
	    unmatch: function() {
	    	slideDrawerDisable();
	    }
	  },
	  {
	    context: 'range_1',
	    match: function() {
	      //responsiveEqualHeight();
	      slideDrawerEnable();
        document.documentElement.id = 'range_1';
        console.log('current range:', MQ.new_context);
	    },unmatch: function() {
	      slideDrawerDisable();
	      //responsiveEqualHeightReset();
	    }
	  },
	  {
	    context: 'range_2',
	    match: function() {
	      //responsiveEqualHeight();
	      slideDrawerEnable();
        document.documentElement.id = 'range_2';
        console.log('current range:', MQ.new_context);
	    },unmatch: function() {
	      slideDrawerDisable();
	      //responsiveEqualHeightReset();
	    }
	  },
	  {
	    context: 'range_3',
	    match: function() {
	      slideDrawerDisable();
	      //responsiveEqualHeight();
        document.documentElement.id = 'range_3';
        console.log('current range:', MQ.new_context);
	    },unmatch: function(){
  	    //responsiveEqualHeightReset();
	    }
	  },
	  {
	    context: 'range_4',
	    match: function() {
	      slideDrawerDisable();
	      //responsiveEqualHeight();
        document.documentElement.id = 'range_4';
        console.log('current range:', MQ.new_context);
	    },unmatch: function(){
  	    //responsiveEqualHeightReset();
	    }
	  },
	  {
	    context: 'range_5',
	    match: function() {
	      slideDrawerDisable();
	      //responsiveEqualHeight();
        document.documentElement.id = 'range_5';
        console.log('current range:', MQ.new_context);
	    }
	  }
	];
	MQ.init(queries);
	var range = MQ.new_context;
	
	if($('body.type-1').length){
		$('.bg-image').cycle({
      speed: 3000,
      timeout: 3000,
      fx: 'fade',
      width: '100%',
      fit: 1
    }); 
	}
	
	if($('body.type-6').length){
		$('object.svg-object').hide(); 
		$('object.svg-object').each(function(index){
			$(this).parent().find('svg').attr("id",'svg'+index);
			var svgId = '#svg'+index;
			var svgImage = '..'+$(this).attr("data");
			Snap.load(svgImage, svgLoaded);
			function svgLoaded(data){
				Snap(svgId).append(data);
				$('path').click(function() {
					var pathId = $(this).attr('id');
					if($('#' + pathId + '[class="active"]').length == 1){
						document.getElementById(pathId).setAttribute("class", "");
						$('.map-popup').hide();
					}else{
						if(document.querySelectorAll("path[class='active']")[0])
							document.querySelectorAll("path[class='active']")[0].setAttribute("class", "");
						document.getElementById(pathId).setAttribute("class", "active");
						$('.map-popup').hide();
						$('.map-popup[data-id*="'+pathId+'"]').show();
					}					
				});
			}
		});//each
		
			var $children = $('#team .team-1 .col-lg-4');
	    for(var i = 0, l = $children.length; i < l; i += 3) {
	      $children.slice(i, i+3).wrapAll('<div class="row"></div>');
	    }
	    
	    var $childrenC = $('#team .team-2 .col-lg-4');
	    for(var i = 0, l = $childrenC.length; i < l; i += 3) {
	      $childrenC.slice(i, i+3).wrapAll('<div class="row"></div>');
	    } 
	    
	    var $childrenB = $('#locations .col-lg-4');
	    for(var i = 0, l = $childrenB.length; i < l; i += 3) {
	      $childrenB.slice(i, i+3).wrapAll('<div class="row"></div>');
	    } 
		
	}
	
	if($('body.type-4').length){
		$('section').each(function(){
			var items = $(this).find('li').length;
			var itemsPerCol = Math.round(items / 2);
			var $children = $(this).find('li');
	    for(var i = 0, l = $children.length; i < l; i += itemsPerCol) {
	      $children.slice(i, i+itemsPerCol).wrapAll('<ul></ul>');
	    }
		});
		
	}
	
	
	
  function slideDrawerEnable(){
    var range = MQ.new_context;
    var trigger = '.snap-trigger-';
    var content = '#content_container';
    var drawer = '.snap-drawer-';
    //var contentBorder = '#content_push';
    $('.snap-drawers').show();
    $(content).addClass('active');
    $(trigger+'left').show();
    $(trigger+'right').show();
    $('#content_container.active').unbind();
    $(trigger+'left').click(function(){
      if($(content+'.open-left').length){
        $(content).stop().animate({marginLeft: '0px'}, function(){
          $(this).removeClass('open-left');
          $(drawer+'left').hide();
              //$(contentBorder).removeClass('open-left');
           $('#content_container.active').unbind();
        });

      }else{
        $(content).addClass('open-left').stop().animate({marginLeft: '200px'}, function(){
           $('#content_container.active').click(function(){
            //alert("click");
            $(content).stop().animate({marginLeft: '0px'}, 200, function(){
              $(this).removeClass('open-left');
              //$(contentBorder).removeClass('open-left');
              $(drawer+'left').hide();
              $('#content_container.active').unbind();
            });//animte close
          });//click
          $(window).on('scroll', function() {
             $(content).stop().animate({marginLeft: '0px'}, 200, function(){
              $(this).removeClass('open-left');
              //$(contentBorder).removeClass('open-left');
              $(drawer+'left').hide();
              $('#content_container.active').unbind();
            });//animte close
          });
        });//animate
        //$(contentBorder).addClass('open-left');
        $(drawer+'left').show();
      }
    });
    $(trigger+'right').click(function(){
      if($(content+'.open-left').length){
        $(content).stop().animate({marginLeft: '0px'}, function(){
          $(this).removeClass('open-left');
          $(drawer+'left').hide();
          //$(contentBorder).removeClass('open-left');
           $('#content_container.active').unbind();
        });
      }
      if($(content+'.open-right').length){
        $(content).stop().animate({marginLeft: '0px'}, function(){
          $(this).removeClass('open-right');
          $(drawer+'left').hide();
              //$(contentBorder).removeClass('open-right');
           $('#content_container.active').unbind();
        });
      }else{
        $(content).addClass('open-right').stop().animate({marginLeft: '-200px'}, function(){
           $(drawer+'left').hide();
           $('#content_container.active').click(function(){
            $(content).stop().animate({marginLeft: '0px'}, 200, function(){
              $(this).removeClass('open-right');
              //$(contentBorder).removeClass('open-right');
              $(drawer+'right').hide();
              $('#content_container.active').unbind();
            });//animte close
          });//click
          $(window).on('scroll', function() {
             $(content).stop().animate({marginLeft: '0px'}, 200, function(){
              $(this).removeClass('open-right');
              //$(contentBorder).removeClass('open-right');
              $(drawer+'right').hide();
              $('#content_container.active').unbind();
            });//animte close
          });
        });//animate
        //$(contentBorder).addClass('open-right');
        $(drawer+'right').show();
      }
    });
  }//snapDrawerEnable

  function slideDrawerDisable(){
    var trigger = '.snap-trigger-';
    var content = '#content_container';
    //var contentBorder = '#content_push';
    $('.snap-drawers').hide();
    $(content).removeClass('active');
    $('#content_container.active').unbind();
  	$(trigger+'left').unbind().hide();
    $(trigger+'right').unbind().hide();
    if($(content+'.open-left').length){
      $(content).stop().animate({marginLeft: '0px'}, function(){
        $(this).removeClass('open-left');
        //$(contentBorder).removeClass('open-left');
      });
    }
    if($(content+'.open-right').length){
      $(content).stop().animate({marginLeft: '0px'}, function(){
        $(this).removeClass('open-right');
        //$(contentBorder).removeClass('open-right');
      });
    }
  }


}); //End Document Ready